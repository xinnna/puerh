# puerh

_Vim color scheme with support for 8-color and 256-color terminals_

---

Inspired by [YuèJiǔ](https://github.com/slugbyte/yuejiu).

![puerh](puerh.png)
